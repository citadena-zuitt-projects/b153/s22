/*

Users:
	_id
	username
	password
	firstName
	lastName
	email
	isAdmin
	billingAddress
	shippingAddress
	contactNumber

Products:
	_id
	name
	sku
	price
	quantity
	category
	description
	reviews

Payments:
	_id 
	userId
	type
	details

Shopping Cart:
	_id
	status
	quantity
	products
	totalAmount

Orders:
	_id
	userId
	billingAddress
	shippingAddress
	payment
	status
	products
	amount
	trackingNumber

*/

